package generic;

import java.security.SecureRandom;
import java.util.Random;

public class StringUtility {

    public static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String LOWER = "abcdefghijklmnopqrstuvwxyz";

    public static final String DIGITS = "0123456789";

    public static final String UPPER_CASE_ALPHANUMERIC = UPPER + DIGITS;
    public static final String LOWER_CASE_ALPHANUMERIC = LOWER + DIGITS;
    public static final String BOTH_CASE_ALPHANUMERIC = UPPER + LOWER + DIGITS;
    public static final String NUMERIC = DIGITS;

    private final Random random;

    private char randomChar(){
        return LOWER_CASE_ALPHANUMERIC.charAt(random.nextInt(LOWER_CASE_ALPHANUMERIC.length()));
    }

    private char randomNumeric(){
        return NUMERIC.charAt(random.nextInt(NUMERIC.length()));
    }

    public String randomAlphaNumeric(int length){
        StringBuilder sb = new StringBuilder();
        int spacer = 0;
        while(length > 0){
            length--;
            sb.append(randomChar());
        }
        return sb.toString();
    }

    public String randomNumeric(int length){
        StringBuilder sb = new StringBuilder();
        int spacer = 0;
        while(length > 0){
            length--;
            sb.append(randomNumeric());
        }
        return sb.toString();
    }

    public StringUtility() {
        this.random = new SecureRandom();
    }
}