package generic;

public interface Values {
    StringUtility stringUtility = new StringUtility();
    String path = "/home/obsessoey/Desktop/KYC testing.xlsx";
    String characters = stringUtility.randomAlphaNumeric(4);
    String numerics = stringUtility.randomNumeric(10);
    String firstName = Excel.getCellValue(path,"Sheet1",1,0);
    String lastName = Excel.getCellValue(path,"Sheet1",1,1);
    String businessName = characters;
    String businessEmail = characters+"@"+characters+".com";
    String website = "http://"+characters+".com";
    String contactNumber = numerics;
    String destination = Excel.getCellValue(path,"Sheet1",1,6);
    String country = Excel.getCellValue(path,"Sheet1",1,7);
    String comment = Excel.getCellValue(path,"Sheet1",1,8);
}
