package generic;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.util.concurrent.TimeUnit;

public class BaseTest implements IAutoConfig{
    public WebDriver driver;

    @BeforeSuite
    public void driver(){
        System.setProperty(CHROME_KEY,CHROME_VALUE);
    }

    @BeforeMethod
    public void driverInitialization(){
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        driver.manage().window().maximize();
    }

    @AfterMethod
    public void close(){
//        driver.quit();
    }
}
