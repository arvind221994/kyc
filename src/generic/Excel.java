package generic;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.FileInputStream;

public class Excel {

    public static String getCellValue(String path, String sheet, int row, int column){
        String value = " ";
        try {
            Workbook workbookFactory = WorkbookFactory.create(new FileInputStream(path));
            value = workbookFactory.getSheet(sheet).getRow(row).getCell(column).toString();
        }catch(Exception e){

        }
        return value;
    }

    public static int getLastCellValue(String path,String sheet){
        int cellCount = 0;
        try{
            Workbook workbookFactory = WorkbookFactory.create(new FileInputStream(path));
            cellCount = workbookFactory.getSheet(sheet).getLastRowNum();
        }catch (Exception e){

        }
        return cellCount;
    }
}
