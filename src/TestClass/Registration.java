package TestClass;

import BaseClass.RegistrationPage;
import generic.BaseTest;
import generic.Excel;
import generic.StringUtility;
import generic.Values;
import org.testng.annotations.Test;

public class Registration extends BaseTest implements Values{
    RegistrationPage registrationPage = new RegistrationPage(driver);

    @Test(priority = 1)
    public void validRegistration(){
        driver.get("https://dashboard.kychub.com/register");

        registrationPage.enterFirstName(firstName);
        registrationPage.enterLasttName(lastName);
        registrationPage.enterBuisenessName(businessName);
        registrationPage.enterBusinessEmail(businessEmail);
        registrationPage.enterBuisenessWebsite(website);
        registrationPage.enterContactNumber(contactNumber);
        registrationPage.enterDesignation(destination);
        registrationPage.selectCountry(country);
        registrationPage.comments(comment);
        registrationPage.registerButton();
    }

    @Test(priority = 2)
    public void inValidRegistration(){
        driver.get("https://dashboard.kychub.com/register");

        if(!firstName.equalsIgnoreCase(null))
        registrationPage.enterFirstName(firstName);
        registrationPage.enterLasttName(lastName);
        registrationPage.enterBuisenessName(businessName);
        registrationPage.enterBusinessEmail(businessEmail);
        registrationPage.enterBuisenessWebsite(website);
        registrationPage.enterContactNumber(contactNumber);
        registrationPage.enterDesignation(destination);
        registrationPage.selectCountry(country);
        registrationPage.comments(comment);
        registrationPage.registerButton();
    }
}
