package BaseClass;

import generic.BasePage;
import junit.framework.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

public class RegistrationPage extends BasePage {

    SoftAssert softAssert = new SoftAssert();
    WebDriverWait wait = new WebDriverWait(driver,5);

    @FindBy(xpath = "//*[@placeholder='Enter First Name']")
    private WebElement firstName;

    @FindBy(xpath = "//*[@placeholder='Enter Last Name']")
    private WebElement lastName;

    @FindBy(xpath = "//*[@placeholder='Enter Business Name']")
    private WebElement businessName;

    @FindBy(xpath = "//*[@placeholder='Enter Business Email']")
    private WebElement businessEmail;

    @FindBy(xpath = "//*[@placeholder='Enter Business Website']")
    private WebElement businessWebsite;

    @FindBy(xpath = "//*[@placeholder='Enter Business Contact Number']")
    private WebElement contactNumber;

    @FindBy(xpath = "//*[@placeholder='Enter Designation']")
    private WebElement designation;

    @FindBy(xpath = "//*[@id=\"wrapper\"]/section/div/app-register/section/div[3]/div[2]/div[2]/form/div[8]/div/select")
    private WebElement country;

    @FindBy(xpath = "//*[@placeholder='Comments']")
    private WebElement comment;

    @FindBy(xpath = "//*[@id=\"wrapper\"]/section/div/app-register/section/div[3]/div[2]/div[2]/form/div[17]/div/button")
    private WebElement registerButton;

    @FindBy(xpath = "//*[@id=\"toast-container\"]/div/div")
    private WebElement toastMessage;

    public RegistrationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void enterFirstName(String enterFirstName){
        firstName.sendKeys(enterFirstName);
    }

    public void enterLasttName(String enterLastName){
        lastName.sendKeys(enterLastName);
    }

    public void enterBuisenessName(String enterBuisenessName){
        businessName.sendKeys(enterBuisenessName);
    }

    public void enterBusinessEmail(String enterBuisenessEmail){
        businessEmail.sendKeys(enterBuisenessEmail);
    }

    public void enterBuisenessWebsite(String enterBuisenessWebsite){
        businessWebsite.sendKeys(enterBuisenessWebsite);
    }

    public void enterContactNumber(String enterContactNumber){
        contactNumber.sendKeys(enterContactNumber);
    }

    public void enterDesignation(String enterDesignation){
        designation.sendKeys(enterDesignation);
    }

    public void selectCountry(String selectCountry){
        country.sendKeys(selectCountry);
    }

    public void comments(String comments){
        comment.sendKeys(comments);
    }

    public void registerButton(){
        registerButton.click();
    }

    public void toastMessage(){
        String actualToastMessage[] = wait.until(ExpectedConditions.visibilityOf(toastMessage)).getText().trim().split("\\.");
        System.out.println(actualToastMessage[0]);
        softAssert.assertEquals(actualToastMessage[0].trim(),"Registration done successfully");
        softAssert.assertAll();
    }
}
